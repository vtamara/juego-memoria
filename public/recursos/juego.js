// Basado en https://www.webtips.dev/memory-game-in-javascript

let selectores = {
	contendorTablero: document.querySelector('.contenedor-tablero'),
	tablero: document.querySelector('.tablero'),
	movimientos: document.querySelector('.movimientos'),
	temporizador: document.querySelector('.temporizador'),
	comenzar: document.querySelector('#comenzar'),
	terminar: document.querySelector('#terminar'),
	gana: document.querySelector('.gana')
}

const estado = {
	juegoIniciado: false,
	tarjetasVolteadas: 0,
	totalMovimientos: 0,
	tiempoTotal: 0,
	bucle: null
}

const resuelveMult = (cad) => {
	let p = cad.indexOf('×')
	let n1 = cad.substr(0, p)
	let n2 = cad.substr(p+1)
	return n1*n2
}

const tarjetasIguales = (t1, t2) => {
	if (t1.includes('×')) {
		r1 = resuelveMult(t1)
	} else {
		r1 = +t1
	}
	if (t2.includes('×')) {
		r2 = resuelveMult(t2)
	} else {
		r2 = +t2
	}
	return (r1 == r2) 
}

const revolver = arreglo => {
	const arregloClonado = [...arreglo]

	for (let indice = arregloClonado.length - 1; indice > 0; indice--) {
		const randomIndex = Math.floor(Math.random() * (indice + 1))
		const original = arregloClonado[indice]

		arregloClonado[indice] = arregloClonado[randomIndex]
		arregloClonado[randomIndex] = original
	}

	return arregloClonado
}

const elegirAleatoriamente = (arreglo, numElementos) => {
	const arregloClonado = [...arreglo]
	const elegidos = []

	for (let indice = 0; indice < numElementos; indice++) {
		const indiceAleatorio = Math.floor(Math.random() * 
			arregloClonado.length)

		elegidos.push(arregloClonado[indiceAleatorio])
		arregloClonado.splice(indiceAleatorio, 1)
	}

	return elegidos
}



const generarJuego = () => {
	const dimension = selectores.tablero.getAttribute('data-dimension')

	if (dimension % 2 !== 0) {
		throw new Error("La dimensión del tablero debe ser un número par.")
	}

	const emojis = ['🥔', '🍒', '🥑', '🌽', '🥕', '🍇', '🍉', '🍌', '🥭', '🍍']
	const num10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

	const factor = Math.floor(Math.random() * 10)+1
	
	const elegidos = elegirAleatoriamente(num10, 
		(dimension * dimension) / 2) 

	let mult1 = []
	let mult2 = []
	for(let i = 0; i < elegidos.length; i++) {
		mult1.push(`${factor*elegidos[i]}`)
		mult2.push(`${factor}×${elegidos[i]}`)
	}
	const elementos = revolver([...mult1, ...mult2])
	const tarjetas = `
	<div class="tablero" data-dimension="${dimension}"
	  style="grid-template-columns: repeat(${dimension}, auto)">
	    ${elementos.map(elemento => `
		<div class="tarjeta">
		    <div class="frente-tarjeta"></div>
		    <div class="respaldo-tarjeta">${elemento}</div>
		</div>
	    `).join('')}
       </div>
    `
	const reconocedor = new DOMParser().parseFromString(
		tarjetas, 'text/html')

	selectores.tablero.replaceWith(reconocedor.querySelector('.tablero'))
	selectores.tablero = document.querySelector('.tablero')
}


const comenzarJuego = () => {
	estado.juegoIniciado = true
	selectores.comenzar.classList.add('deshabilitado')
	selectores.terminar.classList.remove('deshabilitado')

	estado.bucle = setInterval(() => {
		estado.tiempoTotal++

		selectores.movimientos.innerText = 
			`${estado.totalMovimientos} movimientos`
		selectores.temporizador.innerText = `tiempo: ${estado.tiempoTotal} seg.`
	}, 1000)
}

const terminarJuego = () => {
	setTimeout(() => {
		estado.juegoIniciado = false
		selectores.comenzar.classList.remove('deshabilitado')
		selectores.terminar.classList.add('deshabilitado')

		clearInterval(estado.bucle)
		estado.bucle = null
		estado.tiempoTotal = 0
		estado.totalMovimientos = 0
		estado.tarjetasVolteadas = 0

		selectores.gana.innerHTML = '¡Ganaste!'
		selectores.movimientos.innerText = 
			`${estado.totalMovimientos} movimientos`
		selectores.temporizador.innerText = 
			`tiempo: ${estado.tiempoTotal} seg.`
		selectores.contendorTablero.classList.remove('volteado')

		generarJuego()
	}, 1000)
}


const voltearTarjeta = tarjeta => {
	estado.tarjetasVolteadas++
	estado.totalMovimientos++

	if (!estado.juegoIniciado) {
		comenzarJuego()
	}

	if (estado.tarjetasVolteadas <= 2) {
		tarjeta.classList.add('volteada')
	}

	if (estado.tarjetasVolteadas === 2) {
		const tarjetasVolteadas = document.querySelectorAll(
			'.volteada:not(.emparejada)')

		if (tarjetasIguales(tarjetasVolteadas[0].innerText,
			tarjetasVolteadas[1].innerText)) {
			tarjetasVolteadas[0].classList.add('emparejada')
			tarjetasVolteadas[1].classList.add('emparejada')
		}

		setTimeout(() => {
			devolverTarjetas()
		}, 1000)
	}

	// Si no hay más tarjetas por voltear, ganamos el juego
	if (!document.querySelectorAll('.tarjeta:not(.volteada)').length) {
		setTimeout(() => {
			selectores.contendorTablero.classList.add('volteado')
			selectores.gana.innerHTML = `
	    <span class="texto-gana">
		¡Ganaste!<br />
		en <span class="resaltar">${estado.totalMovimientos}</span> movimientos<br />
		en menos de <span class="resaltar">${estado.tiempoTotal}</span> segundos
	    </span>
	`
			clearInterval(estado.bucle)
		}, 1000)
	}
}

const devolverTarjetas = () => {
	document.querySelectorAll('.tarjeta:not(.emparejada)').forEach(
		tarjeta => {
			tarjeta.classList.remove('volteada')
		}
	)

	estado.tarjetasVolteadas = 0
}



const pegarEscuchadoresDeEventos = () => {
	document.addEventListener('click', event => {
		const objetivoEvento = event.target
		const eventoPapa = objetivoEvento.parentElement

		if (objetivoEvento.className.includes('tarjeta') && 
			!eventoPapa.className.includes('volteada')) {
			voltearTarjeta(eventoPapa)
		} else if (objetivoEvento.nodeName === 'BUTTON' && 
			!objetivoEvento.className.includes('deshabilitado') &&
			objetivoEvento.id == 'comenzar'
		) {
			comenzarJuego()
		} else if (objetivoEvento.nodeName === 'BUTTON' && 
			!objetivoEvento.className.includes('deshabilitado') &&
			objetivoEvento.id == 'terminar'
		) {
			terminarJuego()
		}

	})
}


generarJuego()
pegarEscuchadoresDeEventos()
